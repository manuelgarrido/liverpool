var router = require('express').Router(),
    cors = require('cors'),
    ObjectID = require('mongodb').ObjectID;
mongoClient = require('mongodb').MongoClient;

var collection;

//realiza una conexion a la base de datos mongo
mongoClient.connect('mongodb://localhost:27017/liverpool', function (err, db) {

    if (!err) {
        console.log('MongoDB ha sido conectado');
        collection = db.collection('criterios');
    } else {
        console.error('Error de conexion', err);
    }

})

//crea un nuevo registro o actualiza si ya esta el criterio, en la base de datos
router.get('/criterios/:criterio', cors(), function (req, res) {
    "use strict";

    collection.update(
        //criterio a buscar
        { criterio: req.params.criterio },

        //criterio a insertar o actualizar 'upsert'
        {
            $set: {
                criterio: req.params.criterio,
            }
        },

        /*
        * indica que si el registro ya existe en el documento json
        * lo actualice, y si no existe, entonces inserta un nuevo criterio
        */
        { upsert: true, multi: true },

        //devuelve el resultado en formato json y envia un http status 
        // http status 200: ok
        // http status 500: Internal Server Error
        function (error, result) {
            var resultado;
            var status = 200;

            if (!error) {
                
                resultado = result;

            } else {

                status = 500;
                resultado = error;

            }

            //envia respuesta con status en el header
            // el cuerpo del mensaje es en formato json
            res.status(status)
                .type('json')
                .send(JSON.stringify(resultado));

        }

    );




});

//exporta el modulo para poder ser utilizado
module.exports = router;