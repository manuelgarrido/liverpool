import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public resultados;
  public listaCriterios;
  public textoBusquedaName;

  url_guardar_criterios = "http://localhost:3000/criterios/";
  url_liverpool = "https://www.liverpool.com.mx/tienda?d3106047a194921c01969dfdec083925=json&s=";

  constructor(private http: Http) { 

    this.listaCriterios = ['xbox', 'playstation'];

  }

  //Buscamos el valor string que ya trae textoBusqueda
  buscar(textoBusqueda) {

    var url_completa = this.url_liverpool + textoBusqueda;

    //si el parametro viene lleno, entonces realiza la búsqueda
    if (textoBusqueda != "") {
      //consulta el servicio de liverpool
      return this.http.get(url_completa)
        .map(this.extractData)
        .subscribe(
            data => this.resultados = data.contents,
            err => console.log('Error: ', err),
            () => console.log('Busqueda completada')
        );

    } else {
      alert("Por favor introduzca el criterio a buscar");
    }

  }

  guardarCriterios(textoBusqueda){

    var url_completa = this.url_guardar_criterios + textoBusqueda;

    //si el parametro viene lleno, entonces realiza la búsqueda
    if (textoBusqueda != "") {
      //consulta el servicio de liverpool
      return this.http.get(url_completa)
        .map(this.extractData)
        .subscribe(
            data => this.resultados = data.contents,
            err => console.log('Error: ', err),
            () => console.log('Se guardo el criterio con éxito')
        );

    } else {
      alert("Por favor introduzca el criterio a buscar");
    }

  }

  seleccionarCriterio(lista){
    console.log(lista);
    this.textoBusquedaName = lista;
    this.buscar(lista);
    this.guardarCriterios(lista);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body;
  }

}
