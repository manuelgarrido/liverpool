import { LiverpoolPage } from './app.po';

describe('liverpool App', () => {
  let page: LiverpoolPage;

  beforeEach(() => {
    page = new LiverpoolPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
