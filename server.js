var express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    mongoClient = require('mongodb').MongoClient;

var app = express(),
    endpoints = require('./routes/endpoints'),
    routes = express.Router(),
    port = process.env.PORT || 3000;

/**
 * Nos conectamos a la base de datos para comprobar la conexion con mongodb
 */
mongoClient.connect('mongodb://localhost:27017/liverpool', function (err, db) {

    if (!err) {
        console.log('MongoDB ha sido conectado');
    } else {
        console.error('Error de conexion', err);
    }

});

//Parsea el texto como url codificada
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Le indicamos que tome el archivo de endpoints.js para que maneje las rutas
app.use('/', endpoints);

//Manejo de todas las urls con cors(), para no tener problemas con el cross-site
app.use('*', cors());
app.use(routes);

//El servidor escucha el puerto asignado
app.listen(port, "localhost", function () {
    console.log("Escuchando el puerto: ", port);
}).on('error', function (e) {
    console.error("Hay un problema con el puerto, precione ctrl + c, para salir ");
    console.log(e);
});
